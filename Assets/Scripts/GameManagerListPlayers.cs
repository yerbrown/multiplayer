using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerListPlayers : MonoBehaviour
{

    [System.Serializable]
    public class PlayerLayers
    {
        public GameObject player;
        public LayerMask layer;
    }

    public List<PlayerLayers> players = new List<PlayerLayers>();

    public void AsignLayerToCamera(GameObject newPlayer, Camera cam, Cinemachine.CinemachineFreeLook virtualCam)
    {
        for (int i = 0; i < players.Count; i++)
        {
            if (players[i].player != newPlayer)
            {
                cam.cullingMask &= ~(1 << players[i].layer);
            }
        }
    }

    
}