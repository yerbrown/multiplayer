using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldController : Photon.MonoBehaviour
{


    public Material teamA, teamB;

    public GameManager.Team myTeam;

    public int teamInt;
    // Start is called before the first frame update
    void Start()
    {
        AssignMaterial();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (other.gameObject.GetComponent<PhotonView>().isMine == false && PhotonNetwork.connected == true)
            {
                return;
            }
            this.photonView.TransferOwnership(other.GetComponent<PhotonView>().ownerId);

            if (other.GetComponent<Health>().myTeam != myTeam)
            {
                photonView.RPC("AddShieldPoints", PhotonTargets.AllBuffered);
            }
            Debug.Log("Shield pick up by " + other.name);
            PhotonNetwork.Destroy(gameObject);
        }
    }

    public void AssignMaterial()
    {
        if (myTeam == GameManager.gameManagerInstance.myTeam)
        {
            GetComponent<Renderer>().material = teamA;
        }
        else
        {
            GetComponent<Renderer>().material = teamB;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<BoxCollider>().isTrigger = true;
    }

    [PunRPC]
    public void AddShieldPoints()
    {
        GameManager.gameManagerInstance.AddPoints(myTeam);
    }
}
