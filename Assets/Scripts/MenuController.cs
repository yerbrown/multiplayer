﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public string versionName = "0.1";
    public GameObject startButton;
    public GameObject connectPanel;
    public InputField userNameInput;
    public InputField createGameInput;
    public InputField joinGameInput;
    public Text currentUsernameText;

    private void Awake()
    {
        PhotonNetwork.ConnectUsingSettings(versionName);
    }

    private void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby(TypedLobby.Default);
        Debug.Log("Conected");
    }

    public void ChangeUserNameInput()
    {
        if (userNameInput.text.Length>=3)
        {
            startButton.SetActive(true);
        }
        else
        {
            startButton.SetActive(false);
        }
    }


    public void SetUserName()
    {
        connectPanel.SetActive(true);
        PhotonNetwork.playerName = userNameInput.text;
        currentUsernameText.text = PhotonNetwork.playerName;
    }

    public void CreateGame()
    {
        PhotonNetwork.CreateRoom(createGameInput.text, new RoomOptions() { MaxPlayers = 5 }, null);
    }

    public void JoinGame()
    {
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 5;
        PhotonNetwork.JoinOrCreateRoom(joinGameInput.text, roomOptions, TypedLobby.Default);
    }


    private void OnJoinedRoom()
    {
        PhotonNetwork.LoadLevel("MainGame");
    }
}
