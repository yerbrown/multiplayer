﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;
public class CharacterAiming : Photon.MonoBehaviour
{

    public float turnSpeed = 15;
    public float aimDuration = 0.3f;
    public Camera mainCamera;
    public Rig aimLayer;
    public RaycastWeapon weapon;
    public Transform cameraLookAt;
    public Cinemachine.AxisState xAxis;
    public Cinemachine.AxisState yAxis;
    public Cinemachine.CinemachineVirtualCamera cinemachineCustom;
    public MultiAimConstraint[] aimingPose;
    public Transform aimingPosTransform;



    // Start is called before the first frame update
    void Start()
    {
        aimingPosTransform.SetParent(null);
        foreach (MultiAimConstraint aiming in aimingPose)
        {
            var data = aiming.data.sourceObjects;
            data.SetTransform(0, aimingPosTransform);

            aiming.data.sourceObjects = data;

        }
        RigBuilder rigs = GetComponent<RigBuilder>();
        rigs.Build();

        if (photonView.isMine == false && PhotonNetwork.connected == true)
        {
            return;
        }
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        weapon = GetComponentInChildren<RaycastWeapon>();
        cinemachineCustom.Follow = cameraLookAt;
        GetComponentInChildren<RaycastWeapon>().raycastDestination = mainCamera.GetComponentInChildren<CrossHairTarget>();


        aimingPosTransform.name += " " + gameObject.name;
        //aimingPosTransform.SetParent(null);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (photonView.isMine == false && PhotonNetwork.connected == true)
        {
            return;
        }
        xAxis.Update(Time.fixedDeltaTime);
        yAxis.Update(Time.fixedDeltaTime);

        cameraLookAt.eulerAngles = new Vector3(yAxis.Value, xAxis.Value, 0);

        float yawCamera = mainCamera.transform.rotation.eulerAngles.y;
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, yawCamera, 0), turnSpeed * Time.deltaTime);

    }

    private void Update()
    {

        if (photonView.isMine == false && PhotonNetwork.connected == true)
        {
            return;
        }

        aimingPosTransform.position = mainCamera.transform.GetChild(0).position;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Cursor.visible)
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
            else
            {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }

        }

        if (Input.GetMouseButtonDown(1))
        {
            cinemachineCustom.GetComponent<Animator>().SetBool("Aiming", true);
        }
        if (Input.GetMouseButtonUp(1))
        {
            cinemachineCustom.GetComponent<Animator>().SetBool("Aiming", false);
        }


        //cinemachineCustom.m_XAxis.m_InputAxisValue = Input.GetAxisRaw("Mouse X");
        //cinemachineCustom.m_YAxis.m_InputAxisValue = Input.GetAxisRaw("Mouse Y");




    }
    private void LateUpdate()
    {
        if (photonView.isMine == false && PhotonNetwork.connected == true)
        {
            return;
        }

        if (Input.GetMouseButton(0))
        {
            photonView.RPC("StartFire", PhotonTargets.AllBuffered, weapon.raycastDestination.transform.position);
            StartFire(weapon.raycastDestination.transform.position);
        }

        if (Input.GetMouseButtonUp(0))
        {
            photonView.RPC("StopFiring", PhotonTargets.AllBuffered);
            StopFiring();
        }





    }
    [PunRPC]
    public void StartFire(Vector3 hitPos)
    {
        weapon.StartFiring(hitPos);
    }
    [PunRPC]
    public void StopFiring()
    {
        weapon.StopFiring();
    }
}
