using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Health : Photon.MonoBehaviour, IPunObservable
{
    public float health, maxHealth;
    public Text lifeText;
    public Image lifeImg;
    public float damage = 2;

    [Tooltip("The Player's UI GameObject Prefab")]
    [SerializeField]
    public GameObject PlayerUiPrefab;

    public PlayerUI myUI;

    public GameManager.Team myTeam;

    public GameObject chapaPrefabA, chapaPrefabB;

    private PhotonPlayer lastHitEnemy;
    private void Start()
    {
        health = maxHealth;
        if (photonView.isMine == false && PhotonNetwork.connected == true)
        {
            return;
        }
        lifeText = GameManager.gameManagerInstance.lifeText;
        lifeImg = GameManager.gameManagerInstance.lifeImg;
        if (photonView.isMine == false)
        {
            if (PlayerUiPrefab != null)
            {
                GameObject _uiGo = Instantiate(PlayerUiPrefab);
                myUI = _uiGo.GetComponent<PlayerUI>();
                myUI.SetTarget(this);
            }
        }
        UpdateLifeText();
    }
    private void Modifyhealt(float amount)
    {
        health -= amount;
        UpdateLifeText();
        //Check the health  after receiving damage to see if the player is dead
        CheckHealth();
    }

    private void CheckHealth()
    {
        //healthBarIn.fillAmount = playerLifePoints / playerFullLifePoints;

        if (photonView.isMine && health <= 0)
        {
            //GameManager.gameManagerInstance.EnableRespawn();
            //playerScripts.inputEnabled = false;
            Debug.Log(gameObject.name + "is dead");
            Vector3 chapaPos = transform.position;
            photonView.RPC("Die", PhotonTargets.AllBuffered);
            RespawnShield(chapaPos);
            UpdateLifeText();
            GameManager.gameManagerInstance.PlayerDied(photonView.owner, lastHitEnemy, myTeam);
        }
        else
        {
            myUI.Killed();
        }
    }

    [PunRPC]
    public void Die()
    {

        int randomValue = Random.Range(0, 5);
        transform.position = GameManager.gameManagerInstance.respawnPoints[randomValue].position;
        health = maxHealth;
        // GameManager.gameManagerInstance.AddPoints(myTeam);

    }

    public void RespawnShield(Vector3 pos)
    {

        if (myTeam == GameManager.Team.BLUE)
        {
            PhotonNetwork.Instantiate(chapaPrefabA.name, pos + Vector3.up * 3, Quaternion.identity, 0);

        }
        else
        {
            PhotonNetwork.Instantiate(chapaPrefabB.name, pos + Vector3.up * 3, Quaternion.identity, 0);
        }

        //GameManager.gameManagerInstance.DebugText("shield of team " + chapa.GetComponent<ShieldController>().myTeam);

    }

    public void UpdateLifeText()
    {
        if (photonView.isMine == false)
        {
            if (myUI == null)
            {
                GameObject _uiGo = Instantiate(PlayerUiPrefab);
                myUI = _uiGo.GetComponent<PlayerUI>();
                myUI.SetTarget(this);
            }
            else
            {
                myUI.UpdateLifeImg();

            }
        }

        if (photonView.isMine == false && PhotonNetwork.connected == true)
        {
            return;
        }
        lifeText.text = health + "/" + maxHealth + " +";
        lifeImg.fillAmount = (float)health / maxHealth;
    }


    [PunRPC]
    public void ReduceHealth(int userId, PhotonPlayer enemy)
    {
        if (PhotonNetwork.player.ID == userId)
        {
            Modifyhealt(damage);
            lastHitEnemy = enemy;
        }
    }

    public void ReduceHealthLocal()
    {
        Modifyhealt(damage);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            // We own this player: send the others our data

            stream.SendNext(health);
            stream.SendNext(myTeam);

        }
        else
        {
            float lastHealth = health;
            // Network player, receive data
            this.health = (float)stream.ReceiveNext();
            this.myTeam = (GameManager.Team)stream.ReceiveNext();
            if (lastHealth != health)
            {
                UpdateLifeText();
            }

        }
    }

    void OnLevelWasLoaded(int level)
    {
        this.CalledOnLevelWasLoaded(level);
    }



    void CalledOnLevelWasLoaded(int level)
    {
        GameObject _uiGo = Instantiate(PlayerUiPrefab);
        myUI = _uiGo.GetComponent<PlayerUI>();
        myUI.SetTarget(this);
    }

}
