﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShootController : MonoBehaviour
{

    public Transform weaponCanon;
    public GameObject ammo;

    public bool alreadyShooted, aiming;

    public Image reload;
    public GameObject pointer;
    public LayerMask layerMask;
    public Vector3 dir;
    // Update is called once per frame
    void Update()
    {
        if (aiming)
        {
            if (Input.GetMouseButtonDown(0) && alreadyShooted == false)
            {
                GameObject newAmmo = Instantiate(ammo, weaponCanon.transform.position, Quaternion.Euler(Vector3.zero));
                newAmmo.transform.rotation = weaponCanon.transform.rotation;
                alreadyShooted = true;
            }
            if (Input.GetMouseButtonUp(0) && alreadyShooted == true)
            {
                alreadyShooted = false;
            }
        }

        reload.gameObject.SetActive(!alreadyShooted);


        RayShootAim();


    }


    public void RayShootAim()
    {
        RaycastHit hit;

        Ray ray = Camera.main.ScreenPointToRay(new Vector3(0.5f, 0.5f, 0));

        Transform cameraPos = Camera.main.transform;

        if (Physics.Raycast(weaponCanon.transform.position, cameraPos.forward, out hit, 100))
        {
            dir = (hit.point - weaponCanon.transform.position).normalized;
        }

        //Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.forward, Color.green);
        Debug.DrawLine(weaponCanon.transform.position, hit.point, Color.green);
    }
}
