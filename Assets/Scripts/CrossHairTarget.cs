using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossHairTarget : MonoBehaviour
{
    Camera mainCamera;

    Ray ray;
    public RaycastHit hitInfo;

    public bool hitting; 
    // Start is called before the first frame update
    void Start()
    {
        mainCamera = GetComponentInParent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        ray.origin = mainCamera.transform.position;
        ray.direction = mainCamera.transform.forward;
        if (Physics.Raycast(ray, out hitInfo))
        {
            Debug.DrawLine(ray.origin, hitInfo.point, Color.red);
            transform.position = hitInfo.point;
            hitting = true;
        }
        else
        {
            Debug.DrawLine(ray.origin, mainCamera.transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            transform.position = mainCamera.transform.TransformDirection(Vector3.forward) * 1000;
            hitting = false;
        }

    }
}
