﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameManager : MonoBehaviour, IPunObservable
{
    public static GameManager gameManagerInstance;



    public GameObject playerPrefab, aimingTransform;
    public GameObject gameCanvas;
    public GameObject sceneCamera, playerCamera;
    public Cinemachine.CinemachineVirtualCamera virtualCamera;
    public Text pingText, lifeText;
    public Image lifeImg;

    public GameObject playerFeedText;
    public GameObject playerFeedGrid;

    private GameManagerListPlayers listPlayer;

    public List<Transform> respawnPoints = new List<Transform>();

    [Header("Points")]
    public Text textA;
    public Text textB;
    public Text teamText;
    public Image imgA;
    public Image imgB;
    public Image teamImg;

    public Color blueT, redT;

    public Team myTeam;

    public int pointsA;
    public int pointsB;

    public int maxPoints = 50;
    public enum Team
    {
        BLUE,
        RED
    }

    private void Awake()
    {
        gameManagerInstance = this;
        gameCanvas.SetActive(true);
        listPlayer = GetComponent<GameManagerListPlayers>();
    }
    // Update is called once per frame
    void Update()
    {
        pingText.text = "PING: " + PhotonNetwork.GetPing();
    }

    public void SpawnPlayer(int selectedTeam)
    {
        sceneCamera.SetActive(false);
        playerCamera.SetActive(true);
        int randomValue = Random.Range(0, 5);

        GameObject player = PhotonNetwork.Instantiate(playerPrefab.name, respawnPoints[randomValue].position, Quaternion.identity, 0);

        //GameObject aimPos = PhotonNetwork.Instantiate(aimingTransform.name, new Vector2(transform.position.x * randomValue, transform.position.y), Quaternion.identity, 0);
        //player.GetComponent<CharacterAiming>().aimingPosTransform = aimPos.transform;
        player.GetComponent<CharacterAiming>().cinemachineCustom = virtualCamera;
        player.GetComponent<Health>().myTeam = (Team)selectedTeam;
        myTeam = (Team)selectedTeam;
        player.GetComponent<CharacterAiming>().mainCamera = playerCamera.GetComponent<Camera>();
        UpdatePointsUI();
        gameCanvas.SetActive(false);
    }

    public void LeaveRoom()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.LoadLevel("MainMenu");
    }


    private void OnPhotonPlayerConnected(PhotonPlayer player)
    {
        GameObject obj = Instantiate(playerFeedText, new Vector2(0, 0), Quaternion.identity);
        obj.transform.SetParent(playerFeedGrid.transform, false);
        obj.GetComponent<Text>().text = player.NickName + " joined the game";
        obj.GetComponent<Text>().color = Color.green;
    }
    private void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
        GameObject obj = Instantiate(playerFeedText, new Vector2(0, 0), Quaternion.identity);
        obj.transform.SetParent(playerFeedGrid.transform, false);
        obj.GetComponent<Text>().text = player.NickName + " left the game";
        obj.GetComponent<Text>().color = Color.red;
    }
    public void DebugText(string error)
    {
        GameObject obj = Instantiate(playerFeedText, new Vector2(0, 0), Quaternion.identity);
        obj.transform.SetParent(playerFeedGrid.transform, false);
        obj.GetComponent<Text>().text = error;
        obj.GetComponent<Text>().color = Color.blue;
    }

    public void PlayerDied(PhotonPlayer player, PhotonPlayer enemyPlayer, Team killedTeam)
    {
        GameObject obj = Instantiate(playerFeedText, new Vector2(0, 0), Quaternion.identity);
        obj.transform.SetParent(playerFeedGrid.transform, false);
        if (killedTeam == Team.BLUE)
        {
            obj.GetComponent<Text>().text = "<Color=Blue>" + player.NickName + "</Color>" + " killed by  <Color=Red>" + enemyPlayer.NickName + "</Color> ";
        }
        else
        {
            obj.GetComponent<Text>().text = "<Color=Red>" + player.NickName + "</Color>" + " killed by  <Color=Blue>" + enemyPlayer.NickName + "</Color> ";
        }
        obj.GetComponent<Text>().color = Color.white;
    }

    public void AddPoints(Team playerDead, int points = 1)
    {
        if (playerDead == Team.BLUE)
        {
            pointsB += points;
        }
        else
        {
            pointsA += points;
        }
        UpdatePointsUI();
        if (pointsA >= maxPoints || pointsB >= maxPoints)
        {
            LeaveRoom();
        }
    }

    public void UpdatePointsUI()
    {

        teamImg.gameObject.SetActive(true);
        if (myTeam == Team.BLUE)
        {
            teamImg.color = blueT;
            teamText.text = "Blue";
        }
        else
        {
            teamImg.color = redT;
            teamText.text = "Red";
        }


        textA.text = pointsA.ToString("00");
        textB.text = pointsB.ToString("00");
        imgA.fillAmount = (float)pointsA / maxPoints;
        imgB.fillAmount = (float)pointsB / maxPoints;

    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            // We own this player: send the others our data

            stream.SendNext(pointsA);
            stream.SendNext(pointsB);
        }
        else
        {
            float lastPointA = pointsA;
            float lastPointB = pointsB;
            // Network player, receive data
            this.pointsA = (int)stream.ReceiveNext();
            this.pointsB = (int)stream.ReceiveNext();


            if (lastPointA != pointsA || lastPointB != pointsB)
            {
                UpdatePointsUI();
            }

        }
    }
}
