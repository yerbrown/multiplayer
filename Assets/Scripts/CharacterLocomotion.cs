﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterLocomotion : Photon.MonoBehaviour
{
    Animator animator;
    Vector2 input, turnInput;

    CharacterController _controller;
    public float moveSpeed;
    public float jumHeight = 10;
    public bool isJumping;
    public float airControl;
    public float gravity = 9.8f;
    public float stepDown;
    public float jumpDamp;
    public float groundSpeed;
    public float pushPower;
    public Vector3 velocity, rootMotion;

    public LayerMask layer;
    // Start is called before the first frame update
    void Start()
    {
        _controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (photonView.isMine == false && PhotonNetwork.connected == true)
        {
            return;
        }
        input.x = Input.GetAxis("Horizontal");
        input.y = Input.GetAxis("Vertical");

        turnInput.x = Input.GetAxis("Mouse X");

        if (Input.GetButtonDown("Jump"))
        {
            Jump();
        }

        animator.SetFloat("InputX", input.x);
        animator.SetFloat("InputY", input.y);


        animator.SetFloat("Turn", turnInput.x);



    }

    private void FixedUpdate()
    {
        if (photonView.isMine == false && PhotonNetwork.connected == true)
        {
            return;
        }
        if (isJumping)
        {
            UpdateInAir();
        }
        else
        {
            UpdateOnGround();
        }

    }

    private void UpdateOnGround()
    {
        Vector3 stepForwardAmount = rootMotion * groundSpeed;
        Vector3 stepDownAmount = Vector3.down * stepDown;

        _controller.Move(stepForwardAmount + stepDownAmount);
        rootMotion = Vector3.zero;

        if (!_controller.isGrounded)
        {
            SetInAir(0);
        }
    }

    private void UpdateInAir()
    {
        velocity.y -= gravity * Time.deltaTime;
        Vector3 displacement = velocity * Time.fixedDeltaTime;
        displacement += CalculateAirControl();
        _controller.Move(displacement);
        isJumping = !_controller.isGrounded;
        rootMotion = Vector3.zero;
        animator.SetBool("IsJumping", isJumping);
    }

    public void Jump()
    {
        if (!isJumping)
        {
            float jumpVelocity = Mathf.Sqrt(2 * gravity * jumHeight);
            SetInAir(jumpVelocity);
        }
    }

    private void SetInAir(float jumpVelocity)
    {
        isJumping = true;
        velocity = animator.velocity * jumpDamp * groundSpeed;
        velocity.y = jumpVelocity;
        animator.SetBool("IsJumping", true);
    }

    private void OnAnimatorMove()
    {
        if (photonView.isMine == false && PhotonNetwork.connected == true)
        {
            return;
        }
        rootMotion += animator.deltaPosition;

    }

    private Vector3 CalculateAirControl()
    {
        return ((transform.forward * input.y) + (transform.right * input.x)) * (airControl / 100);
    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (photonView.isMine == false && PhotonNetwork.connected == true)
        {
            return;
        }
        Rigidbody body = hit.collider.attachedRigidbody;

        // no rigidbody
        if (body == null || body.isKinematic)
            return;

        // We dont want to push objects below us
        if (hit.moveDirection.y < -0.3f)
            return;

        // Calculate push direction from move direction,
        // we only push objects to the sides never up and down
        Vector3 pushDir = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);

        // If you know how fast your character is trying to move,
        // then you can also multiply the push velocity by that.

        // Apply the push
        body.velocity = pushDir * pushPower;

    }
}
