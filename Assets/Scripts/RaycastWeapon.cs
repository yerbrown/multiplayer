using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastWeapon : Photon.MonoBehaviour
{
    public bool isFiring = false;
    public ParticleSystem[] muzzleFlash;
    public ParticleSystem hitEffect;
    public Transform raycastOrigin;
    public CrossHairTarget raycastDestination;
    Ray ray;
    RaycastHit hitInfo;
    public float rayMaxDistance = 100f;

    public GameObject obstacleMark;

    public float fireRate;
    private float lastShootTime = 0;

    public PhotonView PV;

    private void Start()
    {
        PV = GetComponentInParent<PhotonView>();
    }
    private void Update()
    {
        if (lastShootTime < fireRate)
        {
            lastShootTime += Time.deltaTime;
        }
        else
        {
            lastShootTime = fireRate;
        }
        if (transform.parent.GetComponent<PhotonView>().isMine == false && PhotonNetwork.connected == true)
        {
            return;
        }
        ray.origin = raycastOrigin.position;
        ray.direction = raycastDestination.transform.position - raycastOrigin.position;
        if (Physics.Raycast(ray, out hitInfo, rayMaxDistance))
        {
            Debug.DrawLine(ray.origin, raycastDestination.transform.position, Color.green);


            hitEffect.transform.position = hitInfo.point;
            hitEffect.transform.forward = hitInfo.normal;


        }

        if (hitInfo.collider != raycastDestination.hitInfo.collider)
        {
            if (raycastDestination.hitting)
            {
                obstacleMark.SetActive(true);
                obstacleMark.transform.forward = hitInfo.normal;
                obstacleMark.transform.position = hitInfo.point + obstacleMark.transform.forward * 0.05f;
            }
        }
        else
        {
            obstacleMark.SetActive(false);
        }
    }

    public void StartFiring(Vector3 hitPos)
    {
        isFiring = true;
        if (lastShootTime >= fireRate)
        {
            foreach (var particle in muzzleFlash)
            {
                particle.Emit(1);
            }

            ray.origin = raycastOrigin.position;
            ray.direction = hitPos - raycastOrigin.position;
            if (Physics.Raycast(ray, out hitInfo))
            {
                Debug.DrawLine(ray.origin, hitInfo.point, Color.blue, 1.0f);
                hitEffect.transform.position = hitInfo.point;
                hitEffect.transform.forward = hitInfo.normal;
                hitEffect.Emit(1);


                if (hitInfo.collider.CompareTag("Player"))
                {
                    if (hitInfo.collider.GetComponent<Health>().myTeam != GetComponentInParent<Health>().myTeam)
                    {
                        hitInfo.collider.GetComponentInParent<Health>().photonView.RPC("ReduceHealth", PhotonTargets.All, hitInfo.collider.GetComponent<PhotonView>().ownerId, GetComponentInParent<PhotonView>().owner);
                    }
                }
                else
                {
                    Debug.Log("PLayer no hited");
                }
            }

            lastShootTime = 0;
        }
    }

    public void StopFiring()
    {
        isFiring = false;
    }
}
