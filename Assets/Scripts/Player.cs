﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : Photon.MonoBehaviour
{
    public PhotonView photonView;
    public Rigidbody2D playerRigidbody;
    public GameObject playerCamera;
    public SpriteRenderer playerSpriteRenderer;
    public Text playerName;
    public Animator playerAnimator;

    public bool isGrounded;
    public float moveSpeed;
    public float jumpForce;
    
    private void Awake()
    {
        if (photonView.isMine)
        {
            playerCamera.SetActive(true);
            playerName.text = PhotonNetwork.playerName;
        }
        else
        {
            playerName.text = photonView.owner.NickName;
            playerName.color = Color.magenta;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (photonView.isMine)
        {
            CheckInput();
        }
    }

    private void CheckInput()
    {
        Vector3 move = new Vector3(Input.GetAxisRaw("Horizontal"), 0);
        transform.position += move * moveSpeed * Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.A))
        {
            photonView.RPC("FlipTrue", PhotonTargets.AllBuffered);
        }
        
        if (Input.GetKeyDown(KeyCode.D))
        {
            photonView.RPC("FlipFalse", PhotonTargets.AllBuffered);
        }

        //Also this will be used for the animations
        if (Input.GetKeyDown(KeyCode.A)|| Input.GetKeyDown(KeyCode.D))
        {
            playerAnimator.SetBool("isRunning", true);
        }
        else
        {
            playerAnimator.SetBool("isRunning", false);
        }


    }

    [PunRPC]
    private void FlipTrue()
    {
        playerSpriteRenderer.flipX = true;
    }

    [PunRPC]
    private void FlipFalse()
    {
        playerSpriteRenderer.flipX = false;
    }
}
