﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienMovement_Controller : MonoBehaviour
{

    public CharacterController controller;

    public Transform cam;
    public float speed = 6f;
    public float speedMultiply = 1;
    public float turnSmothTime = 0.1f;
    public float turnSmothVelocity;
    public float gravity = -9.8f;
    public float jumpSpeed;
    public float directionY;
    public Vector3 movementDir;
    public Animator anim;

    [Range(0, 1f)]
    public float DistanceToGround;
    public float rayDistance;
    public LayerMask layerMask;

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horizontal, 0, vertical).normalized;
        float moveForward = new Vector2(horizontal, vertical).magnitude;
        movementDir = Vector3.zero;
        if (controller.isGrounded)
        {
            directionY = -2;
            if (Input.GetButtonDown("Jump"))
            {
                directionY = jumpSpeed;
                //anim.SetTrigger("Jump");
            }
            //anim.SetBool("IsGrounded", true);
        }
        else
        {
            //anim.SetBool("IsGrounded", false);
            directionY += gravity * Time.deltaTime;
        }

        if (direction.magnitude >= 0.1f || Input.GetAxisRaw("Aim") > 0)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, cam.eulerAngles.y, ref turnSmothVelocity, turnSmothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            movementDir = moveDir * moveForward * speed;
            //anim.SetBool("Walk", true);
            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                speedMultiply = 2;
                //anim.SetBool("Run", true);
            }
            if (Input.GetKeyUp(KeyCode.LeftShift))
            {
                speedMultiply = 1;
                //anim.SetBool("Run", false);
            }
        }
        else
        {
            speedMultiply = 1;
            //anim.SetBool("Walk", false);
            //anim.SetBool("Run", false);
        }
        movementDir.y = directionY;

        controller.Move(movementDir * speedMultiply * Time.deltaTime);
        anim.SetFloat("Forward", vertical);
        anim.SetFloat("Rigthward", horizontal);

        
    }

    private void OnAnimatorIK(int layerIndex)
    {
        if (anim)
        {
            anim.SetIKPositionWeight(AvatarIKGoal.LeftFoot, 1);
            anim.SetIKRotationWeight(AvatarIKGoal.LeftFoot, 1);

            anim.SetIKPositionWeight(AvatarIKGoal.RightFoot, 1);
            anim.SetIKRotationWeight(AvatarIKGoal.RightFoot, 1);

            // Left foot
            RaycastHit hit;
            Ray ray = new Ray(anim.GetIKPosition(AvatarIKGoal.LeftFoot) + Vector3.up, Vector3.down * rayDistance);
            if (Physics.Raycast(ray, out hit, DistanceToGround + 1f, layerMask))
            {
                if (hit.transform.tag == "Walkable")
                {
                    Vector3 footPosition = hit.point;
                    footPosition.y += DistanceToGround;
                    anim.SetIKPosition(AvatarIKGoal.LeftFoot, footPosition);
                    //anim.SetIKRotation(AvatarIKGoal.LeftFoot, Quaternion.LookRotation(transform.forward, hit.normal));
                }
            }

            ray = new Ray(anim.GetIKPosition(AvatarIKGoal.RightFoot) + Vector3.up, Vector3.down * rayDistance);
            if (Physics.Raycast(ray, out hit, DistanceToGround + 1f, layerMask))
            {
                if (hit.transform.tag == "Walkable")
                {
                    Vector3 footPosition = hit.point;
                    footPosition.y += DistanceToGround;
                    anim.SetIKPosition(AvatarIKGoal.RightFoot, footPosition);
                    //anim.SetIKRotation(AvatarIKGoal.RightFoot, Quaternion.LookRotation(transform.forward, hit.normal)); 
                }
            }


        }
    }
}
