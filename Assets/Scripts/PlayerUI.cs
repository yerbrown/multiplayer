using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    [Tooltip("UI Text to display Player's Name")]
    [SerializeField]
    private Text playerNameText;


    [Tooltip("UI Slider to display Player's Health")]
    [SerializeField]
    private Image playerHealthImg;

    private Health target;

    float characterControllerHeight = 0f;
    Transform targetTransform;
    Renderer targetRenderer;
    CanvasGroup _canvasGroup;
    Vector3 targetPosition;

    [Tooltip("Pixel offset from the player target")]
    [SerializeField]
    private Vector3 screenOffset = new Vector3(0f, 30f, 0f);
    public Animator anim;


    private float n_DisableCount;
    public float disableTime = 3f;
    private bool disable;
    private void Awake()
    {
        anim = GetComponent<Animator>();

        this.transform.SetParent(GameObject.Find("Canvas").GetComponent<Transform>(), false);

        _canvasGroup = this.GetComponent<CanvasGroup>();
    }



    // Start is called before the first frame update
    void Start()
    {

        // Reflect the Player Health
        if (playerHealthImg != null)
        {
            UpdateLifeImg();
        }

        // Destroy itself if the target is null, It's a fail safe when Photon is destroying Instances of a Player over the network
        if (target == null)
        {
            Destroy(this.gameObject);
            return;
        }
        gameObject.SetActive(false);
    }

    public void UpdateLifeImg()
    {
        gameObject.SetActive(true);
        anim.SetBool("Active", true);
        ResetTimer();
        playerHealthImg.fillAmount = target.health / target.maxHealth;
    }

    private void Update()
    {
        if (disable)
        {
            n_DisableCount -= Time.deltaTime;
            if (n_DisableCount <= 0)
            {
                anim.SetBool("Active", false);
            }
        }

        // Destroy itself if the target is null, It's a fail safe when Photon is destroying Instances of a Player over the network
        if (target == null)
        {
            Destroy(this.gameObject);
            return;
        }
    }
    void LateUpdate()
    {
        // Do not show the UI if we are not visible to the camera, thus avoid potential bugs with seeing the UI, but not the player itself.
        if (targetRenderer != null)
        {
            this._canvasGroup.alpha = targetRenderer.isVisible ? 1f : 0f;
        }


        // #Critical
        // Follow the Target GameObject on screen.
        if (targetTransform != null)
        {
            targetPosition = targetTransform.position;
            targetPosition.y += characterControllerHeight;
            transform.position = Camera.main.WorldToScreenPoint(targetPosition) + screenOffset;
        }
    }

    public void SetTarget(Health _target)
    {
        if (_target == null)
        {
            Debug.LogError("<Color=Red><a>Missing</a></Color> PlayMakerManager target for PlayerUI.SetTarget.", this);
            return;
        }
        // Cache references for efficiency
        target = _target;
        if (playerNameText != null)
        {
            playerNameText.text = target.photonView.owner.NickName;
        }

        targetTransform = this.target.GetComponent<Transform>();
        targetRenderer = this.target.GetComponent<Renderer>();
        CharacterController characterController = _target.GetComponent<CharacterController>();
        // Get data from the Player that won't change during the lifetime of this Component
        if (characterController != null)
        {
            characterControllerHeight = characterController.height;
        }
    }


    private void OnEnable()
    {
        ResetTimer();
    }
    public void ResetTimer()
    {
        n_DisableCount = disableTime;
        disable = true;
    }

    public void Killed()
    {
        disable = false;
        anim.SetBool("Active", false);
    }
}
